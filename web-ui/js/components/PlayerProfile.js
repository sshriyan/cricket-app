var React = require('react'); 

var PlayerProfile = React.createClass({
 
  render: function() { 

    var player = this.props.player;
    var teams = player.majorTeams;

    if (!this.props.player.majorTeams) {
            return null;
    }

    var majorTeams = teams.map(function (majorTeam) {
            return (
                <li className="navigation__item">
                    <a className="navigation__link" href={ majorTeam.teamId }>
                        {majorTeam.team}
                    </a>
                </li>
                );
    });



  	return ( 
       <section className="content"> 

          <div className="box box-primary ">
            
            <div className="box-header with-border">
                  <h3 className="box-title">Profile</h3>
                  <div className="box-tools pull-right">
                    <button className="btn btn-box-tool" data-widget="collapse"><i className="fa fa-minus"></i></button>
                      <button className="btn btn-box-tool dropdown-toggle" data-toggle="dropdown" aria-expanded="true"><i className="fa fa-wrench"></i></button>
                  </div>
            </div>  

            <div className="box-body">
              
                <div className="row"> 
                  <div className="col-md-9">
                    <div className="col-md-9 col-sm-4 invoice-col">
                      <b>Fullname:</b> {player.firstName} {player.lastName} <br/>
                      <b>Born:</b> {player.dob}<br/>  
                      <b>Playing role:</b>{player.playingRole}<br/>  
                      <b>Batting style:</b> {player.battingStyle}<br/>
                      <b>Bowling style:</b> {player.bowlingStyle}<br/>

                      {majorTeams}
                      <br/>

                    </div>
                  </div>
                   <div className="col-md-3">
                    <div className="avatar-view" title="" data-original-title="Change the avatar">
                        
                        <div className="box-body">
                          
                          <img src={player.profileImageLink} alt="User Image"/>
                         
                        </div>
                     </div>
                  </div>
                </div> 
            </div>

          </div>

        </section>
  	);
  },
 
 
});

module.exports = PlayerProfile;