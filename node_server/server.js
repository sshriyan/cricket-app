// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
var player = require('./routes/player');
var contextVersion = "/api/v1"
// configure app to use bodyParser()
// this will let us get the data from a POST 
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
 
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8081;        // set our port
 
app.use(contextVersion, player);

  
// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);  



