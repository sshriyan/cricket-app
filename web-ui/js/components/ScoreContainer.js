var React = require('react');
var Score = require('./Score');  
var Heading = require('./Heading');

var ScoreContainer = React.createClass({
   
 
  render: function() {
    var scoreList = [];
    for (var i = 0; i < 3; i++) {
      scoreList.push(<Score/>);
    }


  	return (
      <scorecontainer>
        <Heading title="Latest Scores"/>
        <div className="row">
            {scoreList}
         </div>
       </scorecontainer>
  	);
  },
 
 
});

module.exports = ScoreContainer;