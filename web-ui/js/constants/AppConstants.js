var appConstants = {
	GET_PLAYER: "GET_PLAYER",
	ADD_PLAYER: "ADD_PLAYER",
  	END_POINT_URL : "http://localhost:8081/api/v1"
};

module.exports = appConstants;