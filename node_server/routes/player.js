
var express    = require('express');      

module.exports = function() {
  
    var player = express.Router();

    player.get('/player/:id', function(req, res) {
        res.json({
            "id": "1",
        	"firstName" : "Kumar",
        	"lastName" : "Sangakkara",
        	"dob" : "October 27, 1977",
            "playingRole" : "Wicketkeeper batsman",
            "battingStyle" : "Left-hand bat",
            "bowlingStyle" : "Right-arm offbreak",
            "profileImageLink" : "http://p.imgci.com/db/PICTURES/CMS/203200/203231.1.jpg",
            
            "majorTeams": [
                    { 
                        "teamId" : "1",
                        "team" : "Sri Lanka"
                    },
                    { 
                        "teamId" : "2",
                        "team" : "Asia XI"
                    },
                    { 
                        "teamId" : "3",
                        "team" : "Central Province"
                    },
                    { 
                        "teamId" : "4",
                        "team" : "Kings XI Punjab"
                    },
                    
                ]  
        	}
        );

    });

    
   return player;

}();