var Api = require('./Api');
var Dispatcher = require('../dispatcher/AppDispatcher');
var AppConstants = require('../constants/AppConstants');


// Define the ActionCreator.
var PlayerActionCreator = { 

	getPlayer: function (id) {
		Api
			.get(AppConstants.END_POINT_URL+'/player/'+id)
			.then(function (player) {
			
				// Dispatch an action containing the categories.
				Dispatcher.handleAction({
					actionType: AppConstants.GET_PLAYER,
					player: player
				});
			});
	}
};


module.exports = PlayerActionCreator;

 