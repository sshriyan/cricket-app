var React = require('react');
var Heading = require('./Heading');
var PlayerProfile = require('./PlayerProfile');
var PlayerStore = require('../stores/PlayerStore');
var PlayerActionCreator = require('../actions/PlayerActionCreator');

var ProfileContainer = React.createClass({

	getInitialState: function(){
    return {
			//player: playerStore.getPlayer(this.props.params.id)
      player: {}
    }
	},

  componentWillMount: function () {
    PlayerStore.addChangeListener(this._onChange);
  },

	componentDidMount: function(){
    PlayerActionCreator.getPlayer(this.props.params.id);
  },
 
 	componentWillUnmount: function(){
    	PlayerStore.removeChangeListener(this._onChange);
  },
    
  _onChange: function(){ 
    	this.setState({
      		player: PlayerStore.getPlayer()
    	})
  },

  render: function() {
  
  		return (
      		<profilecontainer>
        		<Heading title="Shriyan San"  breadCrumb="My Profile"/>
        		<PlayerProfile player={this.state.player}/>
       		</profilecontainer>
  		);
  },
  
});

module.exports = ProfileContainer;