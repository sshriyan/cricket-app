
var React = require('react');  
var Router = require('react-router');
var Route = Router.Route;
var ScoreContainer = require('./Components/ScoreContainer');
var ProfileContainer = require('./Components/ProfileContainer');

var Inbox = React.createClass({   
  render: function () {
    return <h2>Inbox</h2>; 
  }
});  

     
// declare our routes and their hierarchy
var routes = (
  <Route handler={App}>
    <Route path="main" handler={ScoreContainer}/>
    <Route path="myprofile/:id" handler={ProfileContainer}/>
    <Route path="inbox" handler={Inbox}/>
  </Route>  
);
      
   
var RouteHandler = Router.RouteHandler;

var App = React.createClass({
  render () {
    return (
         <RouteHandler/>   
    )
  }
});  
   


Router.run(routes, Router.HashLocation, function (Handler) {
  React.render(<Handler/>, document.getElementById('content-wrapper'));
});
    

