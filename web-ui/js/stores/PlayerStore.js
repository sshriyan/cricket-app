var AppDispatcher = require('../dispatcher/AppDispatcher');
var AppConstants = require('../constants/AppConstants');
var objectAssign = require('react/lib/Object.assign');
var EventEmitter = require('events').EventEmitter;
var CHANGE_EVENT = 'change';

var _store = [];

function setPlayer (player) {
    _store = player;
}

var PlayerStore = objectAssign({}, EventEmitter.prototype, {
    
    emitChange: function () {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function(cb){
        this.on(CHANGE_EVENT, cb);
    },

    removeChangeListener: function(cb){
        this.removeListener(CHANGE_EVENT, cb);
    },
  
    getPlayer: function(){
       return _store;
    },

});

AppDispatcher.register(function(payload){
   
    var action = payload.action;

    switch(action.actionType){
      
        case AppConstants.GET_PLAYER:
                setPlayer(action.player);
               // playerStore.emit(CHANGE_EVENT);
                break;
      
        default:
                return true;
                break;
    }

    PlayerStore.emitChange();
});

module.exports = PlayerStore;