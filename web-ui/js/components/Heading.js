var React = require('react'); 

var Heading = React.createClass({
 
  /**
   * @return {object}
   */
  render: function() { 
    var title = this.props.title;

  	return ( 
      <section className="content-header">  
          <h1>
            {title}
            <small>team name</small>
          </h1>
          <ol className="breadcrumb">
            <li><a href="#"><i className="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">{this.props.breadCrumb}</a></li>
            <li className="active">{title}</li>
          </ol>
      </section>
  
  	);
  },
 
 
});

module.exports = Heading;